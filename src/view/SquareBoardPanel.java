package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import model.Bispo;
import model.Cavalo;
import model.Peao;
import model.Piece;
import model.Rainha;
import model.Rei;
import model.Square;
import model.Torre;
import control.SquareControl;

public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<SquarePanel>();

		initializeSquareControl();
		initializeGrid();
		initializePiecesInChess();
	}

	private void initializeSquareControl() {
		Color colorOne = Color.WHITE;
		Color colorTwo = Color.GRAY;
		Color colorHover = Color.BLUE;
		Color colorSelected = Color.GREEN;

		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,
				colorSelected);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
			square = this.squareControl.getSquareList().get(i);
			gridBag.gridx = square.getPosition().y;
			gridBag.gridy = square.getPosition().x;
			square.getPosition().setLocation(gridBag.gridx, gridBag.gridy);

			SquarePanel squarePanel = new SquarePanel(square);

			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}

	}

	private void initializePiecesInChess() {
		String piecePath = "icon/Brown P_48x48.png";
		Piece peaoB = new Peao(piecePath);
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(1, i).setPiece(peaoB);
		}
		
		piecePath = "icon/Brown R_48x48.png";
		Piece torreB = new Torre(piecePath);
		this.squareControl.getSquare(0, 0).setPiece(torreB);
		this.squareControl.getSquare(0, 7).setPiece(torreB);

		piecePath = "icon/Brown N_48x48.png";
		Piece cavaloB = new Cavalo(piecePath);
		this.squareControl.getSquare(0, 1).setPiece(cavaloB);
		this.squareControl.getSquare(0, 6).setPiece(cavaloB);

		piecePath = "icon/Brown B_48x48.png";
		Piece bispoB = new Bispo(piecePath);
		this.squareControl.getSquare(0, 2).setPiece(bispoB);
		this.squareControl.getSquare(0, 5).setPiece(bispoB);

		piecePath = "icon/Brown Q_48x48.png";
		Piece rainhaB = new Rainha(piecePath);
		this.squareControl.getSquare(0, 3).setPiece(rainhaB);

		piecePath = "icon/Brown K_48x48.png";
		Piece reiB = new Rei(piecePath);
		this.squareControl.getSquare(0, 4).setPiece(reiB);

		
		piecePath = "icon/White P_48x48.png";
		Piece peaoW = new Peao(piecePath);
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(6, i).setPiece(peaoW);
		}
		
		piecePath = "icon/White R_48x48.png";
		Torre torreW = new Torre(piecePath);
		this.squareControl.getSquare(7, 0).setPiece(torreW);
		this.squareControl.getSquare(7, 7).setPiece(torreW);

		piecePath = "icon/White N_48x48.png";
		Cavalo cavaloW = new Cavalo(piecePath);
		this.squareControl.getSquare(7, 1).setPiece(cavaloW);
		this.squareControl.getSquare(7, 6).setPiece(cavaloW);

		piecePath = "icon/White B_48x48.png";
		Bispo bispoW = new Bispo(piecePath);
		this.squareControl.getSquare(7, 2).setPiece(bispoW);
		this.squareControl.getSquare(7, 5).setPiece(bispoW);

		piecePath = "icon/White Q_48x48.png";
		Rainha rainhaW = new Rainha(piecePath);
		this.squareControl.getSquare(7, 3).setPiece(rainhaW);

		piecePath = "icon/White K_48x48.png";
		Rei reiW = new Rei(piecePath);
		this.squareControl.getSquare(7, 4).setPiece(reiW);
	}
}
