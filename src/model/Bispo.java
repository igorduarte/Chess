package model;

import java.awt.Point;
import java.util.ArrayList;

public class Bispo extends Piece {

    private ArrayList<Point> moves;

    public Bispo(Team team) {
        super(team);
        this.moves = new ArrayList<Point>();
    }

    public Bispo(String imagePath) {
		super(imagePath);
	}
    
    @Override
    public ArrayList<Point> getMoves(Square square, ArrayList<Square> squareList) {
        this.moves.clear();

        int i, j;

        Point point;

        i = square.getPosition().x;
        j = square.getPosition().y;
        while (i > 0 && j < 7) {
            point = new Point(0, 0);
            i--;
            j++;

            if (squareList.get(i + j * 8).havePiece()) {
                String color = squareList.get(i + j * 8).getPiece().getImagePath().substring(5, 10);

                if ((color.equals("White") && getTeam() == Team.UP_TEAM) || (color.equals("Brown") && getTeam() == Team.DOWN_TEAM)) {
                    point.setLocation(i, j);
                    moves.add(point);
                }

                break;
            }

            point.setLocation(i, j);
            moves.add(point);
        }

        i = square.getPosition().x;
        j = square.getPosition().y;
        while (i > 0 && j > 0) {
            point = new Point(0, 0);
            i--;
            j--;

            if (squareList.get(i + j * 8).havePiece()) {
                String color = squareList.get(i + j * 8).getPiece().getImagePath().substring(5, 10);

                if ((color.equals("White") && getTeam() == Team.UP_TEAM) || (color.equals("Brown") && getTeam() == Team.DOWN_TEAM)) {
                    point.setLocation(i, j);
                    moves.add(point);
                }

                break;
            }

            point.setLocation(i, j);
            moves.add(point);
        }

        i = square.getPosition().x;
        j = square.getPosition().y;
        while (i < 7 && j < 7) {
            point = new Point(0, 0);
            i++;
            j++;

            if (squareList.get(i + j * 8).havePiece()) {
                String color = squareList.get(i + j * 8).getPiece().getImagePath().substring(5, 10);

                if ((color.equals("White") && getTeam() == Team.UP_TEAM) || (color.equals("Brown") && getTeam() == Team.DOWN_TEAM)) {
                    point.setLocation(i, j);
                    moves.add(point);
                }

                break;
            }

            point.setLocation(i, j);
            moves.add(point);
        }

        i = square.getPosition().x;
        j = square.getPosition().y;
        while (i < 7 && j > 0) {
            point = new Point(0, 0);
            i++;
            j--;

            if (squareList.get(i + j * 8).havePiece()) {
                String color = squareList.get(i + j * 8).getPiece().getImagePath().substring(5, 10);

                if ((color.equals("White") && getTeam() == Team.UP_TEAM) || (color.equals("Brown") && getTeam() == Team.DOWN_TEAM)) {
                    point.setLocation(i, j);
                    moves.add(point);
                }

                break;
            }

            point.setLocation(i, j);
            moves.add(point);
        }

        return this.moves;
    }
}
