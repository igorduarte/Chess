package model;

import java.awt.Point;
import java.util.ArrayList;

public class Cavalo extends Piece {

    private ArrayList<Point> moves;

    public Cavalo(Team team) {
        super(team);
        this.moves = new ArrayList<Point>();
    }

    public Cavalo(String imagePath) {
		super(imagePath);
	}
    
    @Override
    public ArrayList<Point> getMoves(Square square, ArrayList<Square> squareList) {
        this.moves.clear();

        int i = square.getPosition().x;
        int j = square.getPosition().y;

        Point point;

        if ((i - 1) >= 0 && (j + 2) <= 7) {
            if (squareList.get(i - 1 + (j + 2) * 8).havePiece()) {
                String color = squareList.get(i - 1 + (j + 2) * 8).getPiece().getImagePath().substring(5, 10);
                if ((color.equals("White") && getTeam() == Team.UP_TEAM) || (color.equals("Brown") && getTeam() == Team.DOWN_TEAM)) {
                    point = new Point(0, 0);
                    point.setLocation(i - 1, j + 2);
                    moves.add(point);
                }
            } else {
                point = new Point(0, 0);
                point.setLocation(i - 1, j + 2);
                moves.add(point);
            }
        }

        if ((i - 2) >= 0 && (j + 1) <= 7) {
            if (squareList.get((i - 2) + (j + 1) * 8).havePiece()) {
                String color = squareList.get(i - 2 + (j + 1) * 8).getPiece().getImagePath().substring(5, 10);
                if ((color.equals("White") && getTeam() == Team.UP_TEAM) || (color.equals("Brown") && getTeam() == Team.DOWN_TEAM)) {
                    point = new Point(0, 0);
                    point.setLocation(i - 2, j + 1);
                    moves.add(point);
                }
            } else {
                point = new Point(0, 0);
                point.setLocation(i - 2, j + 1);
                moves.add(point);
            }
        }

        if ((i - 2) >= 0 && (j - 1) >= 0) {
            if (squareList.get(i - 2 + (j - 1) * 8).havePiece()) {
                String color = squareList.get(i - 2 + (j - 1) * 8).getPiece().getImagePath().substring(5, 10);
                if ((color.equals("White") && getTeam() == Team.UP_TEAM) || (color.equals("Brown") && getTeam() == Team.DOWN_TEAM)) {
                    point = new Point(0, 0);
                    point.setLocation(i - 2, j - 1);
                    moves.add(point);
                }
            } else {
                point = new Point(0, 0);
                point.setLocation(i - 2, j - 1);
                moves.add(point);
            }
        }

        if ((i - 1) >= 0 && (j - 2) >= 0) {
            if (squareList.get(i - 1 + (j - 2) * 8).havePiece()) {
                String color = squareList.get(i - 1 + (j - 2) * 8).getPiece().getImagePath().substring(5, 10);
                if ((color.equals("White") && getTeam() == Team.UP_TEAM) || (color.equals("Brown") && getTeam() == Team.DOWN_TEAM)) {
                    point = new Point(0, 0);
                    point.setLocation(i - 1, j - 2);
                    moves.add(point);
                }
            } else {
                point = new Point(0, 0);
                point.setLocation(i - 1, j - 2);
                moves.add(point);
            }
        }

        if ((i + 1) <= 7 && (j - 2) >= 0) {
            if (squareList.get(i + 1 + (j - 2) * 8).havePiece()) {
                String color = squareList.get(i + 1 + (j - 2) * 8).getPiece().getImagePath().substring(5, 10);
                if ((color.equals("White") && getTeam() == Team.UP_TEAM) || (color.equals("Brown") && getTeam() == Team.DOWN_TEAM)) {
                    point = new Point(0, 0);
                    point.setLocation(i + 1, j - 2);
                    moves.add(point);
                }
            } else {
                point = new Point(0, 0);
                point.setLocation(i + 1, j - 2);
                moves.add(point);
            }
        }

        if ((i + 2) <= 7 && (j - 1) >= 0) {
            if (squareList.get(i + 2 + (j - 1) * 8).havePiece()) {
                String color = squareList.get(i + 2 + (j - 1) * 8).getPiece().getImagePath().substring(5, 10);
                if ((color.equals("White") && getTeam() == Team.UP_TEAM) || (color.equals("Brown") && getTeam() == Team.DOWN_TEAM)) {
                    point = new Point(0, 0);
                    point.setLocation(i + 2, j - 1);
                    moves.add(point);
                }
            } else {
                point = new Point(0, 0);
                point.setLocation(i + 2, j - 1);
                moves.add(point);
            }
        }

        if ((i + 2) <= 7 && (j + 1) <= 7) {
            if (squareList.get(i + 2 + (j + 1) * 8).havePiece()) {
                String color = squareList.get(i + 2 + (j + 1) * 8).getPiece().getImagePath().substring(5, 10);
                if ((color.equals("White") && getTeam() == Team.UP_TEAM) || (color.equals("Brown") && getTeam() == Team.DOWN_TEAM)) {
                    point = new Point(0, 0);
                    point.setLocation(i + 2, j + 1);
                    moves.add(point);
                }
            } else {
                point = new Point(0, 0);
                point.setLocation(i + 2, j + 1);
                moves.add(point);
            }
        }

        if ((i + 1) <= 7 && (j + 2) <= 7) {
            if (squareList.get(i + 1 + (j + 2) * 8).havePiece()) {
                String color = squareList.get(i + 1 + (j + 2) * 8).getPiece().getImagePath().substring(5, 10);
                if ((color.equals("White") && getTeam() == Team.UP_TEAM) || (color.equals("Brown") && getTeam() == Team.DOWN_TEAM)) {
                    point = new Point(0, 0);
                    point.setLocation(i + 1, j + 2);
                    moves.add(point);
                }
            } else {
                point = new Point(0, 0);
                point.setLocation(i + 1, j + 2);
                moves.add(point);
            }
        }

        return this.moves;
    }
}
