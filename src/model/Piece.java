package model;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

public abstract class Piece {

    public static final int CHESSBOARD_ROW = 8;
    public static final int CHESSBOARD_COL = 8;

    private Team team;
    private String imagePath;

    public static enum Team {

        UP_TEAM, DOWN_TEAM;
    }

    public Piece(Team team) {
        this.team = team;
    }
    
	public Piece(String imagePath) {
		super();
		this.setImagePath(imagePath);
	}

    public abstract ArrayList<Point> getMoves(Square square, ArrayList<Square> squareList);

    public boolean pieceIsOnMyTeam(Piece piece) {
        return this.team == piece.getTeam();
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

}
