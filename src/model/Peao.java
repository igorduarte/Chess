package model;

import java.awt.Point;
import java.util.ArrayList;

public class Peao extends Piece {

    private ArrayList<Point> moves;

    public Peao(Team team) {
        super(team);
        this.moves = new ArrayList<Point>();
    }
    
    public Peao(String imagePath) {
		super(imagePath);
	}

    @Override
    public ArrayList<Point> getMoves(Square square, ArrayList<Square> squareList) {
        this.moves.clear();

        int i = square.getPosition().x;
        int j = square.getPosition().y;

        Point point;

        if (getTeam() == Team.DOWN_TEAM) {

            i = square.getPosition().x;
            j = square.getPosition().y;

            if (square.getPosition().y != 0 && !squareList.get(i + (j - 1) * 8).havePiece()) {
                point = new Point(0, 0);
                point.setLocation(i, j - 1);
                moves.add(point);
            }

            i = square.getPosition().x;
            j = square.getPosition().y;

            if (square.getPosition().y == 6 && !squareList.get(i + (j - 2) * 8).havePiece() && !squareList.get(i + (j - 1) * 8).havePiece()) {
                point = new Point(0, 0);
                point.setLocation(i, j - 2);
                moves.add(point);
            }

            i = square.getPosition().x;
            j = square.getPosition().y;
            if (i != 0 && j != 0) {
                if (squareList.get(i - 1 + (j - 1) * 8).havePiece()) {
                    String color = squareList.get(i - 1 + (j - 1) * 8).getPiece().getImagePath().substring(5, 10);
                    if (color.equals("Brown")) {
                        point = new Point(0, 0);
                        point.setLocation(i - 1, j - 1);
                        moves.add(point);
                    }
                }
            }

            i = square.getPosition().x;
            j = square.getPosition().y;
            if (i != 7 && j != 0) {
                if (squareList.get(i + 1 + (j - 1) * 8).havePiece()) {
                    String color = squareList.get(i + 1 + (j - 1) * 8).getPiece().getImagePath().substring(5, 10);
                    if (color.equals("Brown")) {
                        point = new Point(0, 0);
                        point.setLocation(i + 1, j - 1);
                        moves.add(point);
                    }
                }
            }

        } else {
            i = square.getPosition().x;
            j = square.getPosition().y;
            if (j != 7) {
                if (!squareList.get(i + (j + 1) * 8).havePiece()) {
                    point = new Point(0, 0);
                    point.setLocation(i, j + 1);
                    moves.add(point);
                }
            }

            if (square.getPosition().y == 1) {
                if (j == 1) {
                    if (!squareList.get(i + (j + 1) * 8).havePiece() && !squareList.get(i + (j + 2) * 8).havePiece()) {
                        point = new Point(0, 0);
                        point.setLocation(i, j + 2);
                        moves.add(point);
                    }
                }
            }

            if (i != 0 && j != 7) {
                if (squareList.get(i - 1 + (j + 1) * 8).havePiece()) {

                    String color = squareList.get(i - 1 + (j + 1) * 8).getPiece().getImagePath().substring(5, 10);

                    if (color.equals("White")) {
                        point = new Point(0, 0);
                        point.setLocation(i - 1, j + 1);
                        moves.add(point);
                    }
                }
            }

            if (i != 7 && j != 7) {
                if (squareList.get(i + 1 + (j + 1) * 8).havePiece()) {
                    
                    String color = squareList.get(i + 1 + (j + 1) * 8).getPiece().getImagePath().substring(5, 10);
                    
                    if(color.equals("White")){
                        point = new Point(0, 0);
                        point.setLocation(i + 1, j + 1);
                        moves.add(point);
                    }
                }
            }

        }

        return this.moves;
    }
}
