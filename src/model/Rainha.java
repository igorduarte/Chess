package model;

import java.awt.Point;
import java.util.ArrayList;

public class Rainha extends Piece {

    private ArrayList<Point> moves, movesAux;

    public Rainha(Team team) {
        super(team);
        this.moves = new ArrayList<Point>();
    }

    public Rainha(String imagePath) {
		super(imagePath);
	}
    
    @Override
    public ArrayList<Point> getMoves(Square square, ArrayList<Square> squareList) {
        this.moves.clear();

        Point point;

        Bispo bispo=new Bispo(getTeam());
        Torre torre=new Torre(getTeam());
     
        movesAux=bispo.getMoves(square, squareList);
        
        for(int i=0; i<movesAux.size(); i++){
            moves.add(movesAux.get(i));
        }
        
        movesAux=torre.getMoves(square, squareList);
        
        for(int i=0; i<movesAux.size(); i++){
            moves.add(movesAux.get(i));
        }       

        return this.moves;
    }
}
