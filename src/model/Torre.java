package model;

import java.awt.Point;
import java.util.ArrayList;

public class Torre extends Piece {

    private ArrayList<Point> moves;

    public Torre(Team team) {
        super(team);
        this.moves = new ArrayList<Point>();
    }
    
    public Torre(String imagePath) {
		super(imagePath);
	}

    @Override
    public ArrayList<Point> getMoves(Square square, ArrayList<Square> squareList) {
        this.moves.clear();

        Point point;

        int i, j;

        i = square.getPosition().x;
        j = square.getPosition().y;
        while (i < 7) {
            point = new Point(0, 0);
            i++;

            if (squareList.get(i + j * 8).havePiece()) {
                String color = squareList.get(i + j * 8).getPiece().getImagePath().substring(5, 10);
                if ((color.equals("White") && getTeam() == Team.UP_TEAM) || (color.equals("Brown") && getTeam() == Team.DOWN_TEAM)) {
                    point.setLocation(i, j);
                    moves.add(point);
                }
                break;
            }

            point.setLocation(i, j);
            moves.add(point);
        }

        i = square.getPosition().x;
        j = square.getPosition().y;
        while (i > 0) {
            point = new Point(0, 0);
            i--;

            if (squareList.get(i + j * 8).havePiece()) {
                String color = squareList.get(i + j * 8).getPiece().getImagePath().substring(5, 10);
                if ((color.equals("White") && getTeam() == Team.UP_TEAM) || (color.equals("Brown") && getTeam() == Team.DOWN_TEAM)) {
                    point.setLocation(i, j);
                    moves.add(point);
                }
                break;
            }

            point.setLocation(i, j);
            moves.add(point);
        }

        i = square.getPosition().x;
        j = square.getPosition().y;
        while (j < 7) {
            point = new Point(0, 0);
            j++;

            if (squareList.get(i + j * 8).havePiece()) {
                String color = squareList.get(i + j * 8).getPiece().getImagePath().substring(5, 10);
                if ((color.equals("White") && getTeam() == Team.UP_TEAM) || (color.equals("Brown") && getTeam() == Team.DOWN_TEAM)) {
                    point.setLocation(i, j);
                    moves.add(point);
                }
                break;
            }

            point.setLocation(i, j);
            moves.add(point);
        }

        i = square.getPosition().x;
        j = square.getPosition().y;
        while (j > 0) {
            point = new Point(0, 0);
            j--;

            if (squareList.get(i + j * 8).havePiece()) {
                String color = squareList.get(i + j * 8).getPiece().getImagePath().substring(5, 10);
                if ((color.equals("White") && getTeam() == Team.UP_TEAM) || (color.equals("Brown") && getTeam() == Team.DOWN_TEAM)) {
                    point.setLocation(i, j);
                    moves.add(point);
                }
                break;
            }

            point.setLocation(i, j);
            moves.add(point);
        }

        return this.moves;
    }
}
