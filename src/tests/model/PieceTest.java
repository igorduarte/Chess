package tests.model;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import model.Peao;
import model.Piece;
import model.Piece.Team;
import model.Square;
import model.Torre;
import model.Bispo;
import model.Cavalo;
import model.Rainha;
import model.Rei;

import org.junit.Assert;
import org.junit.Test;

import control.SquareControl;

import tests.helper.MovesTestHelper;

public class PieceTest {

	private static final Class<?> CLASSE_PEAO = Peao.class;
	private static final Class<?> CLASSE_TORRE = Torre.class;
	private static final Class<?> CLASSE_CAVALO = Cavalo.class;
	private static final Class<?> CLASSE_BISPO = Bispo.class;
	private static final Class<?> CLASSE_REI = Rei.class;
	private static final Class<?> CLASSE_RAINHA = Rainha.class;

	@Test
	public void testMovePawn() throws Exception {		
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;
		
		Square square = new Square(x, y);
		SquareControl squareControl = new SquareControl();
		ArrayList<Square> squareList= squareControl.getSquareList();

		Piece pieceUpTeam = newPieceInstance(CLASSE_PEAO, Team.UP_TEAM);
		Piece pieceDownTeam = newPieceInstance(CLASSE_PEAO, Team.DOWN_TEAM);

		assertMoves(MovesTestHelper.getPawnMoves(Team.UP_TEAM),
				pieceUpTeam.getMoves(square, squareList));
		assertMoves(MovesTestHelper.getPawnMoves(Team.DOWN_TEAM),
				pieceDownTeam.getMoves(square, squareList));
	}

	@Test
	public void testMoveTower() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Square square = new Square(x, y);
		SquareControl squareControl = new SquareControl();
		ArrayList<Square> squareList= squareControl.getSquareList();
		
		Piece piece = newPieceInstance(CLASSE_TORRE, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getTowerMoves(), piece.getMoves(square, squareList));
	}

	@Test
	public void testMoveHorse() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Square square = new Square(x, y);
		SquareControl squareControl = new SquareControl();
		ArrayList<Square> squareList= squareControl.getSquareList();

		
		Piece piece = newPieceInstance(CLASSE_CAVALO, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getHorseMoves(), piece.getMoves(square, squareList));
	}

	@Test
	public void testMoveBishop() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Square square = new Square(x, y);
		SquareControl squareControl = new SquareControl();
		ArrayList<Square> squareList= squareControl.getSquareList();
		
		Piece piece = newPieceInstance(CLASSE_BISPO, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getBishopMoves(), piece.getMoves(square, squareList));
	}

	@Test
	public void testMoveKing() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Square square = new Square(x, y);
		SquareControl squareControl = new SquareControl();
		ArrayList<Square> squareList= squareControl.getSquareList();
		
		Piece piece = newPieceInstance(CLASSE_REI, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getKingMoves(), piece.getMoves(square, squareList));
	}

	@Test
	public void testMoveQueen() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Square square = new Square(x, y);
		SquareControl squareControl = new SquareControl();
		ArrayList<Square> squareList= squareControl.getSquareList();
		
		Piece piece = newPieceInstance(CLASSE_RAINHA, Team.UP_TEAM);
		ArrayList<Point> moves = new ArrayList<Point>();

		moves.addAll(MovesTestHelper.getTowerMoves());
		moves.addAll(MovesTestHelper.getBishopMoves());

		assertMoves(moves, piece.getMoves(square, squareList));
	}

	private Piece newPieceInstance(Class<?> classPiece, Team team)
			throws Exception {
		return (Piece) classPiece.getDeclaredConstructor(Team.class)
				.newInstance(team);
	}

	private void assertMoves(ArrayList<Point> movesA, ArrayList<Point> movesB)
			throws Exception {
		if (movesA.size() != movesB.size()) {
			Assert.assertTrue(false);
			return;
		}

		for (Point point : movesA) {
			if (!movesB.contains(point)) {
				Assert.assertTrue(false);
				return;
			}
		}

		Assert.assertTrue(true);
	}
}
