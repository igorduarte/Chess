package control;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Bispo;
import model.Cavalo;
import model.Peao;
import model.Piece;
import model.Piece.Team;
import model.Rainha;
import model.Rei;

import model.Square;
import model.Square.SquareEventListener;
import model.Torre;
import util.WrongSquareChosenException;

public class SquareControl implements SquareEventListener {

    public static final int ROW_NUMBER = 8;
    public static final int COL_NUMBER = 8;

    public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
    public static final Color DEFAULT_COLOR_TWO = Color.GRAY;
    public static final Color DEFAULT_COLOR_HOVER = Color.BLUE;
    public static final Color DEFAULT_COLOR_SELECTED = Color.GREEN;

    public static final Square EMPTY_SQUARE = null;

    private Color colorOne;
    private Color colorTwo;
    private Color colorHover;
    private Color colorSelected;

    private Square selectedSquare;
    private ArrayList<Square> squareList;
    private ArrayList<Point> moves;
    private ArrayList<Square> listOfSelectedSquares;

    public SquareControl() {
        this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
                DEFAULT_COLOR_SELECTED);
    }

    public SquareControl(Color colorOne, Color colorTwo, Color colorHover,
            Color colorSelected) {
        this.colorOne = colorOne;
        this.colorTwo = colorTwo;
        this.colorHover = colorHover;
        this.colorSelected = colorSelected;

        this.squareList = new ArrayList<>();
        this.listOfSelectedSquares = new ArrayList<>();
        createSquares();
    }

    public void resetColor(Square square) {
        int index = this.squareList.indexOf(square);
        int row = index / COL_NUMBER;
        int col = index % COL_NUMBER;

        square.setColor(getGridColor(row, col));
    }

    @Override
    public void onHoverEvent(Square square) {
        square.setColor(this.colorHover);
    }

    int atual = 0;

    @Override
    public void onSelectEvent(Square square) {
        int x = square.getPosition().x;
        int y = square.getPosition().y;
        System.out.println("(x " + x + ", y " + y + ")");

        if (haveSelectedCellPanel()) {
            if (!listOfSelectedSquares.contains(square) && !this.selectedSquare.equals(square)) {
                return;
            }
            if (!this.selectedSquare.equals(square) && listOfSelectedSquares.contains(square)) {
                try {
                    if (((checkOfWhite(square) && atual % 2 == 0)) || (checkOfBrown(square) && atual % 2 != 0)) {
                        System.out.println(checkOfBrown(square));
                        throw new WrongSquareChosenException();
                    }
                } catch (WrongSquareChosenException d) {
                    if ((selectedSquare.getPiece().getImagePath().substring(5, 10).equals("White") && atual % 2 == 0) || (selectedSquare.getPiece().getImagePath().substring(5, 10).equals("Brown") && atual % 2 != 0)) {
                        JOptionPane.showMessageDialog(null, "Cheque!");
                    }
                }

                try {
                    if ((atual % 2 == 0 && this.selectedSquare.getPiece().getImagePath().substring(5, 10).equals("White"))
                            || (atual % 2 != 0 && this.selectedSquare.getPiece().getImagePath().substring(5, 10).equals("Brown"))) {
                        atual++;
                        moveContentOfSelectedSquare(square);
                        promoverPeao(square);
                        listOfSelectedSquares.clear();
                    } else {
                        throw new WrongSquareChosenException("Quadrado indisponível!");
                    }
                } catch (WrongSquareChosenException w) {
                    JOptionPane.showMessageDialog(null, "Espere sua vez!");
                }
            } else {
                unselectSquare(square);
            }
        } else {
            selectSquare(square);
        }
    }

    public void promoverPeao(Square square){
    	if(square.getPosition().y==0 && square.getPiece().getImagePath().equals("icon/White P_48x48.png")){
        	String piecePath = "icon/White Q_48x48.png";
    		Piece rainhaW = new Rainha(piecePath);
        	square.setPiece(rainhaW);
        }
        if(square.getPosition().y==7 && square.getPiece().getImagePath().equals("icon/Brown P_48x48.png")){
        	String piecePath = "icon/Brown Q_48x48.png";
    		Piece rainhaB = new Rainha(piecePath);
        	square.setPiece(rainhaB);
        }
    }
    
    @Override
    public void onOutEvent(Square square
    ) {
        if (this.selectedSquare != square && !listOfSelectedSquares.contains(square)) {
            resetColor(square);
        } else {
            square.setColor(Color.CYAN);
        }
    }

    public Square getSquare(int row, int col) {
        return this.squareList.get((row * COL_NUMBER) + col);
    }

    public ArrayList<Square> getSquareList() {
        return this.squareList;
    }

    public Color getGridColor(int row, int col) {
        if ((row + col) % 2 == 0) {
            return this.colorOne;
        } else {
            return this.colorTwo;
        }
    }

    private void addSquare() {
        Square square = new Square();
        this.squareList.add(square);
        resetColor(square);
        resetPosition(square);
        square.setSquareEventListener(this);
    }

    private void resetPosition(Square square) {
        int index = this.squareList.indexOf(square);
        int row = index / COL_NUMBER;
        int col = index % COL_NUMBER;

        square.getPosition().setLocation(row, col);
    }

    private boolean haveSelectedCellPanel() {
        return this.selectedSquare != EMPTY_SQUARE;
    }

    private void moveContentOfSelectedSquare(Square square) {
        square.setPiece(this.selectedSquare.getPiece());
        this.selectedSquare.removePiece();
        unselectSquare(square);
    }

    private void selectSquare(Square square) {
        if (square.havePiece()) {
            this.selectedSquare = square;
            this.selectedSquare.setColor(this.colorSelected);
            this.showPossibleMoves(square);
        }
    }

    private void unselectSquare(Square square) {
        for (int i = 0; i < listOfSelectedSquares.size(); i++) {
            resetColor(listOfSelectedSquares.get(i));
        }
        resetColor(this.selectedSquare);
        listOfSelectedSquares.clear();
        this.selectedSquare = EMPTY_SQUARE;
    }

    private void createSquares() {
        for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
            addSquare();
        }
    }

    private boolean checkOfWhite(Square square) {
        String pieceNome = this.selectedSquare.getPiece().getImagePath().substring(11, 12);

        if (pieceNome.equalsIgnoreCase("Q")) {
            Piece piece = new Rainha(Team.DOWN_TEAM);
            moves = piece.getMoves(square, squareList);

            for (int i = 0; i < moves.size(); i++) {
                if (getSquare(moves.get(i).y, moves.get(i).x).havePiece()) {
                    if (getSquare(moves.get(i).y, moves.get(i).x).getPiece().getImagePath().equals("icon/Brown K_48x48.png")) {
                        getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.YELLOW);
                        listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
                        return true;
                    }
                }
            }
        }

        if (pieceNome.equalsIgnoreCase("B")) {
            Piece piece = new Bispo(Team.DOWN_TEAM);
            moves = piece.getMoves(square, squareList);

            for (int i = 0; i < moves.size(); i++) {
                if (getSquare(moves.get(i).y, moves.get(i).x).havePiece()) {
                    if (getSquare(moves.get(i).y, moves.get(i).x).getPiece().getImagePath().equals("icon/Brown K_48x48.png")) {
                        getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.YELLOW);
                        listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
                        return true;
                    }
                }
            }
        }

        if (pieceNome.equalsIgnoreCase("N")) {
            Piece piece = new Cavalo(Team.DOWN_TEAM);
            moves = piece.getMoves(square, squareList);

            for (int i = 0; i < moves.size(); i++) {
                if (getSquare(moves.get(i).y, moves.get(i).x).havePiece()) {
                    if (getSquare(moves.get(i).y, moves.get(i).x).getPiece().getImagePath().equals("icon/Brown K_48x48.png")) {
                        getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.YELLOW);
                        listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
                        return true;
                    }
                }
            }
        }

        if (pieceNome.equalsIgnoreCase("P")) {
            Piece piece = new Peao(Team.DOWN_TEAM);
            moves = piece.getMoves(square, squareList);

            for (int i = 0; i < moves.size(); i++) {
                if (getSquare(moves.get(i).y, moves.get(i).x).havePiece()) {
                    if (getSquare(moves.get(i).y, moves.get(i).x).getPiece().getImagePath().equals("icon/Brown K_48x48.png")) {
                        getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.YELLOW);
                        listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
                        return true;
                    }
                }
            }
        }

        if (pieceNome.equalsIgnoreCase("R")) {
            Piece piece = new Torre(Team.DOWN_TEAM);
            moves = piece.getMoves(square, squareList);

            for (int i = 0; i < moves.size(); i++) {
                if (getSquare(moves.get(i).y, moves.get(i).x).havePiece()) {
                    if (getSquare(moves.get(i).y, moves.get(i).x).getPiece().getImagePath().equals("icon/Brown K_48x48.png")) {
                        getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.YELLOW);
                        listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean checkOfBrown(Square square) {
        String pieceNome = this.selectedSquare.getPiece().getImagePath().substring(11, 12);

        if (pieceNome.equalsIgnoreCase("Q")) {
            Piece piece = new Rainha(Team.UP_TEAM);
            moves = piece.getMoves(square, squareList);

            for (int i = 0; i < moves.size(); i++) {
                if (getSquare(moves.get(i).y, moves.get(i).x).havePiece()) {
                    if (getSquare(moves.get(i).y, moves.get(i).x).getPiece().getImagePath().equals("icon/White K_48x48.png")) {
                        getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.YELLOW);
                        listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
                        return true;
                    }
                }
            }
        }

        if (pieceNome.equalsIgnoreCase("B")) {
            Piece piece = new Bispo(Team.UP_TEAM);
            moves = piece.getMoves(square, squareList);

            for (int i = 0; i < moves.size(); i++) {
                if (getSquare(moves.get(i).y, moves.get(i).x).havePiece()) {
                    if (getSquare(moves.get(i).y, moves.get(i).x).getPiece().getImagePath().equals("icon/White K_48x48.png")) {
                        getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.YELLOW);
                        listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
                        return true;
                    }
                }
            }
        }

        if (pieceNome.equalsIgnoreCase("N")) {
            Piece piece = new Cavalo(Team.UP_TEAM);
            moves = piece.getMoves(square, squareList);

            for (int i = 0; i < moves.size(); i++) {
                if (getSquare(moves.get(i).y, moves.get(i).x).havePiece()) {
                    if (getSquare(moves.get(i).y, moves.get(i).x).getPiece().getImagePath().equals("icon/White K_48x48.png")) {
                        getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.YELLOW);
                        listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
                        return true;
                    }
                }
            }
        }

        if (pieceNome.equalsIgnoreCase("P")) {
            Piece piece = new Peao(Team.UP_TEAM);
            moves = piece.getMoves(square, squareList);

            for (int i = 0; i < moves.size(); i++) {
                if (getSquare(moves.get(i).y, moves.get(i).x).havePiece()) {
                    if (getSquare(moves.get(i).y, moves.get(i).x).getPiece().getImagePath().equals("icon/White K_48x48.png")) {
                        getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.YELLOW);
                        listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
                        return true;
                    }
                }
            }
        }

        if (pieceNome.equalsIgnoreCase("R")) {
            Piece piece = new Torre(Team.UP_TEAM);
            moves = piece.getMoves(square, squareList);

            for (int i = 0; i < moves.size(); i++) {
                if (getSquare(moves.get(i).y, moves.get(i).x).havePiece()) {
                    if (getSquare(moves.get(i).y, moves.get(i).x).getPiece().getImagePath().equals("icon/White K_48x48.png")) {
                        getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.YELLOW);
                        listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void movesPawnWhite(Square square) {
        Piece piece = new Peao(Team.DOWN_TEAM);

        moves = piece.getMoves(square, squareList);

        for (int i = 0; i < moves.size(); i++) {
            getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.CYAN);
            listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
        }
    }

    private void movesPawnBrown(Square square) {
        Piece piece = new Peao(Team.UP_TEAM);

        moves = piece.getMoves(square, squareList);

        for (int i = 0; i < moves.size(); i++) {
            getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.CYAN);
            listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
        }
    }

    private void movesRookWhite(Square square) {
        Piece piece = new Torre(Team.DOWN_TEAM);

        moves = piece.getMoves(square, squareList);

        for (int i = 0; i < moves.size(); i++) {
            getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.CYAN);
            listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
        }
    }

    private void movesRookBrown(Square square) {
        Piece piece = new Torre(Team.UP_TEAM);

        moves = piece.getMoves(square, squareList);

        for (int i = 0; i < moves.size(); i++) {
            getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.CYAN);
            listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
        }
    }

    private void movesBishopWhite(Square square) {
        Piece piece = new Bispo(Team.DOWN_TEAM);

        moves = piece.getMoves(square, squareList);

        for (int i = 0; i < moves.size(); i++) {
            getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.CYAN);
            listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
        }
    }

    private void movesBishopBrown(Square square) {
        Piece piece = new Bispo(Team.UP_TEAM);

        moves = piece.getMoves(square, squareList);

        for (int i = 0; i < moves.size(); i++) {
            getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.CYAN);
            listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
        }
    }

    private void movesKnightWhite(Square square) {
        Piece piece = new Cavalo(Team.DOWN_TEAM);

        moves = piece.getMoves(square, squareList);

        for (int i = 0; i < moves.size(); i++) {
            getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.CYAN);
            listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
        }
    }

    private void movesKnightBrown(Square square) {
        Piece piece = new Cavalo(Team.UP_TEAM);

        moves = piece.getMoves(square, squareList);

        for (int i = 0; i < moves.size(); i++) {
            getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.CYAN);
            listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
        }
    }

    private void movesQueenWhite(Square square) {
        Piece piece = new Rainha(Team.DOWN_TEAM);

        moves = piece.getMoves(square, squareList);

        for (int i = 0; i < moves.size(); i++) {
            getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.CYAN);
            listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
        }
    }

    private void movesQueenBrown(Square square) {
        Piece piece = new Rainha(Team.UP_TEAM);

        moves = piece.getMoves(square, squareList);

        for (int i = 0; i < moves.size(); i++) {
            getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.CYAN);
            listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
        }
    }

    private void movesKingWhite(Square square) {
        Piece piece = new Rei(Team.DOWN_TEAM);

        moves = piece.getMoves(square, squareList);

        for (int i = 0; i < moves.size(); i++) {
            getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.CYAN);
            listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
        }
    }

    private void movesKingBrown(Square square) {
        Piece piece = new Rei(Team.UP_TEAM);

        moves = piece.getMoves(square, squareList);

        for (int i = 0; i < moves.size(); i++) {
            getSquare(moves.get(i).y, moves.get(i).x).setColor(Color.CYAN);
            listOfSelectedSquares.add(getSquare(moves.get(i).y, moves.get(i).x));
        }
    }

    private void showPossibleMoves(Square square) {
        if (square.getPiece().getImagePath().equals("icon/White P_48x48.png")) {
            movesPawnWhite(square);
        }

        if (square.getPiece().getImagePath().equals("icon/Brown P_48x48.png")) {
            movesPawnBrown(square);
        }

        if (square.getPiece().getImagePath().equals("icon/White R_48x48.png")) {
            movesRookWhite(square);
        }

        if (square.getPiece().getImagePath().equals("icon/Brown R_48x48.png")) {
            movesRookBrown(square);
        }

        if (square.getPiece().getImagePath().equals("icon/White B_48x48.png")) {
            movesBishopWhite(square);
        }

        if (square.getPiece().getImagePath().equals("icon/Brown B_48x48.png")) {
            movesBishopBrown(square);
        }

        if (square.getPiece().getImagePath().equals("icon/White N_48x48.png")) {
            movesKnightWhite(square);
        }

        if (square.getPiece().getImagePath().equals("icon/Brown N_48x48.png")) {
            movesKnightBrown(square);
        }

        if (square.getPiece().getImagePath().equals("icon/White Q_48x48.png")) {
            movesQueenWhite(square);
        }

        if (square.getPiece().getImagePath().equals("icon/Brown Q_48x48.png")) {
            movesQueenBrown(square);
        }

        if (square.getPiece().getImagePath().equals("icon/White K_48x48.png")) {
            movesKingWhite(square);
        }

        if (square.getPiece().getImagePath().equals("icon/Brown K_48x48.png")) {
            movesKingBrown(square);
        }
    }

}
