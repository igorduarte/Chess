package util;

public class WrongSquareChosenException extends Exception {

    private static final long serialVersionUID = 1L;
    
    public WrongSquareChosenException(){
        super();
    }
    
    public WrongSquareChosenException(String message){
        super(message);
    }
    
    public WrongSquareChosenException(String message, Throwable cause){
        super(message, cause);
    }
    
    public WrongSquareChosenException(Throwable cause){
        super(cause);
    }

}
